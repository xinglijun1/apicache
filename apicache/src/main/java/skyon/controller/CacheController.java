package skyon.controller;

import java.io.File;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.net.URLDecoder;
import java.util.ArrayList;
import java.util.Enumeration;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Properties;

import javax.annotation.PostConstruct;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.apache.commons.io.FileUtils;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.http.HttpEntity;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpMethod;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.util.MultiValueMap;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.client.RestTemplate;

import com.alibaba.fastjson.JSONObject;

@RestController
public class CacheController {
	private static final String UTF_8 = "utf-8";

	//缓存文件中的key-value分隔符
	private static final String SPLIT = " = ";

	//load cache
	@PostConstruct
	public void load() throws FileNotFoundException, IOException {
		if(!file.exists()) file.createNewFile();
		List<String> list = FileUtils.readLines(file,UTF_8);
		for(String line : list) {
			String kv[] = line.split(SPLIT, 2);
			prop.put(kv[0], kv[1]);
		}
		System.out.println("load cache from file "+file+", row "+prop.size());
	
	}
	
	RestTemplate restTemplate =new RestTemplate();
	
	@Value("${api.server}")
	String server;

	private File file = new File("cache.properties");

	private Properties prop = new Properties();
	
	//代理任意请求
	@RequestMapping(path = "/**")
	public void forward(HttpServletRequest request, HttpServletResponse response) throws IOException {
		//System.out.println(request.getMethod() +" "+request.getRequestURI());
		ResponseEntity<String> resEntity;
		
		//获取header信息
        HttpHeaders requestHeaders = new HttpHeaders();
        Enumeration<String> headerNames = request.getHeaderNames();
        while (headerNames.hasMoreElements()) {
          String key = (String) headerNames.nextElement();
          String value = request.getHeader(key);
          requestHeaders.add(key, value);
        }
        
        //获取body
        String body = null;
       byte[] by= new byte[10240000];//TODO 假定请求不超10m
       int len = request.getInputStream().read(by);
       if(len>0)
        body = new String(by, 0, len);
		HttpEntity<String> requestEntity = new HttpEntity<String>(body, requestHeaders);

		//url rewrite
		String url = server +request.getRequestURI() ;
		if(request.getQueryString()!=null ) {
			url += "?" +URLDecoder.decode( request.getQueryString(),UTF_8);
		}
		System.out.println(url);
		
		//handle request, use cache or visit api server
		String key =url;
		if(prop.containsKey(key)) {
			//use cache
			System.out.println("use cache "+key);
			resEntity = parseEntity(prop.getProperty(key));
		}else {
			//visit api 
			HttpMethod method = HttpMethod.resolve(request.getMethod());
			resEntity = restTemplate.exchange(url, method, requestEntity, String.class);
			 
			//and save to cache
			prop.put(key, JSONObject.toJSONString(resEntity));
			List<String> lines = new ArrayList<String>();
			for(Object k : prop.keySet()) {
				lines.add(k +SPLIT + prop.getProperty(k.toString()));
			}
			FileUtils.writeLines(file, lines);
			System.out.println("save cache "+key);
			System.out.println("save to file"+ file+", row" + lines.size()) ;
		}
		
		//write response
		response.setStatus(resEntity.getStatusCodeValue());//status code
		HttpHeaders headers = resEntity.getHeaders();	//headers
		for(String headK: headers.keySet()) {
			for(String headV : headers.get(headK)) {
				response.addHeader(headK,  headV);	
			}
		}
		response.setCharacterEncoding(UTF_8);
		response.getWriter().write(resEntity.getBody())	;	//body
		response.flushBuffer();
	}
	
	/**
	 * parse entity. 直接解析出错，需要特别处理
	 * @param entity
	 * @return
	 */
	static ResponseEntity<String> parseEntity(String entity){
		System.out.println(entity);
		JSONObject js = JSONObject.parseObject(entity);
		HttpStatus status = HttpStatus.valueOf(js.getString("statusCode"));
		HttpHeaders headers = JSONObject.parseObject(js.getString("headers"), HttpHeaders.class);
		String body = js.getString("body");
		ResponseEntity<String> en= new ResponseEntity<String>(body, headers,status);
		return en;
	}	

	
}
