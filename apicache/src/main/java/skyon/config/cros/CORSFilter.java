package skyon.config.cros;

import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Component;

import javax.servlet.*;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;

@Component
public class CORSFilter implements Filter {
	@Value("${cors.allowHeaders}")
	private String allowHeaders;

	//was8.5发布时，不能使用接口的默认实现，否则会报AbstractError错误
	@Override
	public void init(FilterConfig filterConfig) throws ServletException {
		 System.out.println("CORSFilter init.");
	}
	@Override
	public void destroy() {
	 
	}
	
    @Override
    public void doFilter(ServletRequest request, ServletResponse response, FilterChain chain) throws IOException, ServletException {
        HttpServletResponse res = (HttpServletResponse) response;
        res.addHeader("Access-Control-Allow-Credentials", "true");
        res.addHeader("Access-Control-Allow-Origin", "*");
        res.addHeader("Access-Control-Allow-Methods", "GET, POST, DELETE, PUT");
        res.addHeader("Access-Control-Allow-Headers", allowHeaders);
        //TODO to config header
        res.addHeader("Access-Control-Allow-Headers", "*");
        if (((HttpServletRequest) request).getMethod().equals("OPTIONS")) {
            response.getWriter().println("ok");
            return;
        }
        chain.doFilter(request, response);
    }
}
