package skyon.controller;

import java.io.IOException;

import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

@RestController
public class DemoController {
	
	public static class Person{
		String name;
		int age;
		public String getName() {
			return name;
		}
		public void setName(String name) {
			this.name = name;
		}
		public int getAge() {
			return age;
		}
		public void setAge(int age) {
			this.age = age;
		}
	}
	
	@RequestMapping(path = "/a/b/c")
	public String demo(String hello) throws IOException {
		return "hello,"+hello;
	}
	
	@PostMapping(path="/save")
	public String save(@RequestBody Person p) {
		return "saved."+p.name;
	}
}
